# Gitlab Acceptance tests

A test suite to verify the Alpine Linux Gitlab instance is still working as
expected.

It provides a docker image with a bats[0] test suite and a docker-compose file
to run this image against the Gitlab instance (which is also running in docker
with docker-compose).

## How to use

1. Create a `.env` file with `GITLAB_ACCESS_TOKEN` set to a gitlab token with
   api scope
2. Create an ssh keypair in `./data/ssh/`, which is added to a user in gitlab
3. Run `docker-compose run test`
